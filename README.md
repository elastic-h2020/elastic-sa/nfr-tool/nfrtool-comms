# README #

This demonstration shows the NFR tool (communication dimension) performing various tasks, such as:
- read the system configuration from dataClay in order to parametrize the KonnektBox Telemetry component
- listen to KonnektBox Telemetry (to obtain advanced network statistics)
- alert/publish the Global Resource Manager (GRM) in case of NFR violations


**Important: the dataclay and konnektbox-telemetry folders are Git submodules and must be initialized by calling**
```
git submodule update --init --recursive
```
**IMMEDIATELY AFTER cloning this repository (first time), otherwise**
```
git submodule update --remote --recursive
```


# Structure
- The **app** folder contains the NFR Tool for Communications
- The **konnektbox-telemetry** folder is associated to the GitLab repo https://gitlab.bsc.es/elastic-h2020/elastic-sa/fog-platform/konnektbox-telemetry.git
- The **dataclay** folder should correspond to the GitLab repo https://gitlab.bsc.es/elastic-h2020/elastic-sa/nfr-tool/dataclay  


# SETUP  
There are two options:
## 1. In the device/machine
To use this demo, a connection to dataClay is mandatory. Before establishing this connection, run the **setupForDataclay.sh** script to configure some necessary settings.  
You can check usage by executing:  
```
./setupForDataclay.sh -h
```

In this demo, **Global Resource Manager (GRM)** (https://gitlab.bsc.es/elastic-h2020/elastic-sa/nfr-tool/grm-global_resource_manager) must be running to create a fake ElasticSystem with several Nodes, COMPSsApplications, Workers and CommunicationLinks. See the README.md file in the GRM repository for further details.

Start dataClay, destroying any dataClay instance that could be active in the device: 
```
docker-compose -f master-dataclay.yml -f backend-dataclay.yml down -v --remove-orphans
```

If this Node is the **"master"** node, run:  
``` 
docker-compose -f master-dataclay.yml up --build   
```
If this Node is one of the **backend** nodes, execute:  
``` 
docker-compose -f backend-dataclay.yml up --build   
```

**NOTE:** In release 2.5, dcinitializer should only start when all dataClay nodes are up! You can use development versions to add nodes "on demand".


Then start the Global Resource Manager (from the respective respositories), **wait for the dcinitializer to finish**, and start the NFR Tools on the different Nodes, executing:  
``` 
docker-compose down  
docker-compose up --build   
```

## 2. Through Nuvla 
Available soon

# Warnings
### Clean volumes from host directory
Between runs, and in order to avoid dataClay exceptions when registering the NFR model, remove every file from **/opt/dataclay/** and then, restart dataClay.  
Also ensure that **dcinitializer** exited with code 0 OR, at least, printed a message similar to 
```
[dataClay] Data model registered in namespace: ElasticNFR
```

### Check dependencies
If you followed the second approach, the **dataClay standalone** application is using the current development version of dataClay (allowing the registration of models in Git), so check (in your pom.xml files) if your applications are using 2.6.1-SNAPSHOT dependency of dataClay, i.e.
```
<dependency>
	<groupId>es.bsc.dataclay</groupId>
        <artifactId>dataclay</artifactId>
        <version>2.6.1-SNAPSHOT</version>
</dependency>
<repositories>
        <repository>
                <id>oss.sonatype.org-snapshot</id>
                <url>http://oss.sonatype.org/content/repositories/snapshots</url>
                <snapshots>
                        <enabled>true</enabled>
                </snapshots>
        </repository>
</repositories>
```
### Check accounts
If during deployment the error 
```
[dataClay] ERROR Invalid account
```
is raised, check that the user accounts are named equally in the configuration and .yml files (-USER=${USER:-defaultUser}). If they are not, put it in as well. 

# Demo behavior
The NFR Tool will search for a fake Elastic System with alias "system".
Then, after reading the system setup from dataClay, the CommunicationLinks information is sent to the KonnektBox Telemetry Daemon, which monitors the required network attributes. The Telemetry component sends the monitored communication metrics back to the NFR Tool for Comms., which computes the communication cost and publishes NFRViolations in the "violations" queue name (topic) to GRM's evaluation.


# Contributors

* dataClay has been developed by Barcelona Supercomputing Center (BSC)
* NFRTool is being jointly developed by ISEP and IKERLAN (Member of the Basque Research & Technology Alliance)
* NFRTool-Comms has been developed by IKERLAN (Member of the Basque Research & Technology Alliance)

# Support

* Please contact Jacobo Fanjul (jfanjul@ikerlan.es) for support and troubleshooting

# Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.
