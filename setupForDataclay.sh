#!/bin/sh

if [ $# -ne 2 ]
then
    printf "Illegal number of parameteres supplied!\nRun $0 -h to check usage.\n"
    exit 1
fi

if [ $1 = '-h' ]
then 
	printf "If this Node has dataClay's logicModule, use:\n\t $0 LM <user>\n If this Node is a dataClay Node backend, use:\n\t $0 <ipLogicModule> <user>\n"
    exit 0
fi

# Search for Ethernet IP address
ip=$(ifconfig | grep -A 1 "^e" | grep inet |  awk '{print $2}')

if [ -z $ip ]
then
	# Search for Wifi IP address
	ip=$(ifconfig | grep -A 1 "^w" | grep inet |  awk '{print $2}')
	if [ -z $ip ]
	then
		echo -n "Ethernet and Wifi IP addresses not found. Insert a valid IP address of your machine:\n"
		read answer
		ip=$answer
		printf "Your IP address is: %s\n" $ip
	fi
fi

# Insert IP address of machine in config files
sed -i -e "s/- DATASERVICE_HOST=.*/- DATASERVICE_HOST=$ip/" *.yml

if [ $1 != 'LM' ] && [ $1 != 'lm' ] 
then
	printf "Entered IP address of logicModule: %s\n" $1
	ip=$1
fi

echo "Account=$2
Password=defaultPass
DataSets=defaultDS
DataSetForStore=defaultDS
StubsClasspath=./stubs" | tee ./model/cfgfiles/session.properties > /dev/null

echo "Account=$2
Password=defaultPass
DataSets=defaultDS
DataSetForStore=defaultDS
StubsClasspath=../dataclay/stubs" | tee ./app/cfgfiles/session.properties > /dev/null

#public_ip=$(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'"' '{ print $2}')

echo "HOST=$ip
TCPPORT=11034" > ./model/cfgfiles/client.properties

sed -i -e "s/- LOGICMODULE_HOST=.*/- LOGICMODULE_HOST=$ip/" *.yml

sed -i -e "s/- USER=.*/- USER=$2/" *.yml

sed -i -e "s/- CENTRAL_MQ_URL=.*/- CENTRAL_MQ_URL=tcp:\/\/$ip:1884/" *.yml

