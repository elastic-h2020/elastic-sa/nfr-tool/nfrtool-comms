#!/bin/bash

# Activate nfrtool-comms or just deploy dataclay
just_dataclay=0

while [ -n "$1" ]; do
	case $1 in
		--) shift, break;;
		-*) case $1 in
			-dc|--dataclay-only) just_dataclay=1; echo "Just dataclay being deployed"; shift;;
		esac
	esac
	shift
done

echo "Removing old dockers and cleaning environment"
docker-compose -f backend-dataclay.yml kill
docker-compose -f backend-dataclay.yml down -v --remove-orphans
docker-compose down -v
sudo rm -rf /opt/dataclay-nfr/*
mkdir -p /opt/dataclay-nfr

dataclay_yml="backend-dataclay.yml"
echo "PARAMS: $#"
if [ $# -eq 1 ];
then
        dataclay_yml=$1
fi

echo "Deploying dataclay dockers"
docker-compose -f $dataclay_yml up -d

# to run the simulator, debug, or whatever
./get_stubs.sh
jar cvf stubs.jar -C stubs .
mvn install:install-file -Dfile=stubs.jar -DgroupId=es.bsc.compss \
                         -DartifactId=nfrtool-dataclay-stubs      \
                         -Dversion=1.0 -Dpackaging=jar            \
                         -DcreateChecksum=true

# Get dataclay.jar from docker and install it
tmpfiledataclay=$(mktemp /tmp/dataclay.XXXXXX)
rm -f $PWD/dataclay.jar
ID=$(docker ps | grep nfrtool-comms_dsjava | awk '{print $1}')
docker cp $ID:/home/dataclayusr/dataclay/dataclay.jar $tmpfiledataclay
mvn install:install-file -Dfile=${tmpfiledataclay} -DgroupId=es.bsc.dataclay \
                         -DartifactId=dataclay -Dversion=2.6.1-SNAPSHOT \
                         -Dpackaging=jar -DcreateChecksum=true
rm -rf "${tmpfiledataclay}"


# Deploy NFR COMMS
if [ $just_dataclay -eq 0 ];
then
	echo "Deploying also NFR COMMS"
	docker-compose up --build -d
fi
