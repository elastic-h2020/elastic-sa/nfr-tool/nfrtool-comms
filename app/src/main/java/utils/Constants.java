package utils;

public class Constants {

    public final static String CONFIG_FILE_PATH = "./files/commscost-config.yml";
    public final static String APP_ATTRIB_PATH = "./files/app-attr.yml";
    public final static String APP_INFO_PATH = "./files/app-info.yml";
    public final static int PERIOD = 5 * 1000;
    public final static String NODE_NAME = "NODE1";

}
