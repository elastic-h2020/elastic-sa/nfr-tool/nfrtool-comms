package utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.SequenceNode;

public class FileUtils {

    Yaml yaml;
    MappingNode rootYml;

    public FileUtils() {

        this.yaml = new Yaml();
    }

    public Node findNode(MappingNode root, List<String> paths) {

        if (paths.isEmpty())
            return root;

        String path = paths.get(0);

        final List<String> next = paths.subList(1, paths.size());

        for (NodeTuple child : root.getValue()) {
            if (child.getKeyNode() instanceof ScalarNode) {
                ScalarNode scalar = (ScalarNode) child.getKeyNode();
                if (scalar.getValue().equals(path)) {
                    if(child.getValueNode() instanceof MappingNode ||  child.getValueNode() instanceof SequenceNode ) {
                        switch(child.getValueNode().getNodeId()) {
                            case mapping:
                                return findNode((MappingNode)child.getValueNode(), next);
                            case sequence:
                                SequenceNode s = (SequenceNode) child.getValueNode();
                                for(Node n : s.getValue()) {
                                    if(n instanceof MappingNode)
                                        if(!next.isEmpty())
                                            return findNode((MappingNode)n, next);
                                }
                                return child.getValueNode();
                            default:
                                break;
                        }

                    }
                    else if(child.getValueNode() instanceof ScalarNode) {
                        return child.getValueNode();
                    }
                }
            }
        }
        return null;
    }

    private Node getYmlNode(String filePath, List<String> ymlPath) {

        MappingNode rootYml;

        try {

            rootYml = (MappingNode) yaml.compose(new FileReader(filePath));
            return findNode(rootYml, ymlPath);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getYmlNodeScalar(String filePath, List<String> ymlPath) {


        Node node = getYmlNode(filePath, ymlPath);

        switch(node.getNodeId()) {
            case scalar:
                ScalarNode scalarNode = (ScalarNode) node;
                return scalarNode.getValue();
            default:
                return null;
        }
    }

    public List<String> getYmlNodeSequence(String filePath, List<String> ymlPath) {

        Node node = getYmlNode(filePath, ymlPath);

        switch(node.getNodeId()) {
            case sequence:
                SequenceNode sequenceNode= (SequenceNode) node;

                return
                        sequenceNode.getValue().stream()
                                .filter(ScalarNode.class::isInstance)
                                .map(scalarNode -> (ScalarNode) scalarNode)
                                .map(ScalarNode::getValue)
                                .collect(Collectors.toList());

            default:
                return null;
        }
    }

    public Yaml getYaml() {
        return yaml;
    }
}
