package nfrTool;

import java.util.Timer;

import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.api.DataClayException;
import org.eclipse.paho.client.mqttv3.MqttException;
import resourceManager.ResourceManagerCommsTask;

public class NFRMonitor {

    private final ElasticSystem system;
    private final long period;

    public NFRMonitor(ElasticSystem system, long period) {
        this.system = system;
        this.period = period;
    }

    public void runMonitor() {
        final Timer timer = new Timer();

        try {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    System.out.println("Exiting NFRTool");
                    timer.cancel();
                    DataClay.finish();
                } catch (DataClayException e) {
                    // e.printStackTrace();
                    // Do nothing
                }
            }));

            ResourceManagerCommsTask mTask = new ResourceManagerCommsTask(this.system);
            timer.scheduleAtFixedRate(mTask, 0, this.period);
        } catch (MqttException e) {
            System.err.println("A problem occurred with the MQTT connection");
            e.printStackTrace();
            System.exit(1);
        } catch (ResourceManagerCommsTask.NodeNotFoundException e) {
            System.err.println("This node could not be found in the ElasticSystem");
            e.printStackTrace();
            System.exit(1);
        }
    }
}
