package nfrTool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.CommunicationLink;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;
import es.bsc.compss.nfr.model.Worker;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.api.DataClayException;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;
import utils.Constants;

public class NFRTool {

    private static final String SYSTEM_ALIAS = "system";

    private static ElasticSystem initializeMockData() {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // App A definition:
        String name = "AppA";
        String uuid = "A";
        final int monitoringPeriod = 1;
        String infoNature = "delaySensitive";

        COMPSsApplication appA = new COMPSsApplication(name, uuid, monitoringPeriod);
        appA.setInfoNature(infoNature);

        // App B definition:
        name = "AppB";
        uuid = "B";
        infoNature = "TCPservices";

        COMPSsApplication appB = new COMPSsApplication(name, uuid, monitoringPeriod);
        appB.setInfoNature(infoNature);

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Node 1 definition:
        /*String ipWifi = "182.168.137.17";
        String ipLte = "172.10.0.2";
        //String ipEth = "192.168.137.17"; // TODO: check value
        String ipEth = "10.0.2.15"; // TODO: check value
        float cpuThreshold = 0;
        float energyThreshold = 0;
        float signalWifi = -30;
        int numCores = 4;

        Node node1 = new Node(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);*/

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Node 1 definition:
        // String ipWifi = "172.17.46.244";
        //String ipWifi = "192.168.67.88";
        String ipWifi = "10.0.2.15";
        String ipLte = "172.10.0.2";
        //String ipEth = "192.168.137.17"; // TODO: check value
        String ipEth1 = Optional.ofNullable(System.getenv("IP_ETH_NODE1")).orElse("192.168.137.13"); // TODO: check value
        float cpuThreshold = 0;
        float energyThreshold = 0;
        float signalWifi = -30;
        int numCores = 4;

        Node node1 = new Node(ipWifi, ipEth1, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        // Node 2 definition:
        ipWifi = "182.168.137.179";
        ipLte = "172.10.0.4";
        String ipEth2 = Optional.ofNullable(System.getenv("IP_ETH_NODE2")).orElse("192.168.137.231"); // TODO: check value
        cpuThreshold = 0;
        energyThreshold = 0;
        signalWifi = -30;
        numCores = 4;

        Node node2 = new Node(ipWifi, ipEth2, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        // Node 3 definition:
        ipWifi = "182.168.137.1";
        ipLte = "172.10.0.6";
        String ipEth3 = Optional.ofNullable(System.getenv("IP_ETH_NODE3")).orElse("172.16.124.67"); // TODO: check value
        cpuThreshold = 0;
        energyThreshold = 0;
        signalWifi = -30;
        numCores = 4;

        Node node3 = new Node(ipWifi, ipEth3, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Worker 1A definition:
        final int pid1A = 32145;
        final boolean active1A = true;
        final float cpuUsage1A = 0;
        final float energyUsage1A = 0;
        final int computingUnits1A = 4;
        final float communicationCost1A = 0;

        /*Worker worker1A = new Worker(node1, pid1A, active1A, appA, cpuUsage1A, energyUsage1A, computingUnits1A, node1.getIpEth(),
                communicationCost1A, new ArrayList<>());*/
        Worker worker1A = new Worker(node1, pid1A, active1A, appA, cpuUsage1A, energyUsage1A, computingUnits1A, node1.getIpWifi(),
                communicationCost1A, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        appA.addWorker(worker1A);

        // Worker 1B definition:
        final int pid1B = 32146;
        final boolean active1B = true;
        final float cpuUsage1B = 0;
        final float energyUsage1B = 0;
        final int computingUnits1B = 4;
        final float communicationCost1B = 0;

        /*Worker worker1B = new Worker(node1, pid1B, active1B, appB, cpuUsage1B, energyUsage1B, computingUnits1B, node1.getIpEth(),
                communicationCost1B, new ArrayList<>());*/
        Worker worker1B = new Worker(node1, pid1B, active1B, appB, cpuUsage1B, energyUsage1B, computingUnits1B, node1.getIpWifi(),
                communicationCost1B, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        appB.addWorker(worker1B);

        // Worker 2A definition:
        final int pid2A = 32147;
        final boolean active2A = true;
        final float cpuUsage2A = 0;
        final float energyUsage2A = 0;
        final int computingUnits2A = 4;
        final float communicationCost2A = 0;

        Worker worker2A = new Worker(node2, pid2A, active2A, appA, cpuUsage2A, energyUsage2A, computingUnits2A, node2.getIpEth(),
                communicationCost2A, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        //node2.addWorker(worker2A);
        appA.addWorker(worker2A);

        // Worker 2B definition:
        final int pid2B = 32148;
        final boolean active2B = true;
        final float cpuUsage2B = 0;
        final float energyUsage2B = 0;
        final int computingUnits2B = 4;
        final float communicationCost2B = 0;

        Worker worker2B = new Worker(node2, pid2B, active2B, appB, cpuUsage2B, energyUsage2B, computingUnits2B, node2.getIpEth(),
                communicationCost2B, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        //node2.addWorker(worker2B);
        appB.addWorker(worker2B);

        // Worker 3A definition:
        final int pid3A = 32149;
        final boolean active3A = true;
        final float cpuUsage3A = 0;
        final float energyUsage3A = 0;
        final int computingUnits3A = 4;
        final float communicationCost3A = 0;

        Worker worker3A = new Worker(node3, pid3A, active3A, appA, cpuUsage3A, energyUsage3A, computingUnits3A, node3.getIpEth(),
                communicationCost3A, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        //node3.addWorker(worker3A);
        appA.addWorker(worker3A);

        // Worker 3B definition:
        final int pid3B = 32150;
        final boolean active3B = true;
        final float cpuUsage3B = 0;
        final float energyUsage3B = 0;
        final int computingUnits3B = 4;
        final float communicationCost3B = 0;

        Worker worker3B = new Worker(node3, pid3B, active3B, appB, cpuUsage3B, energyUsage3B, computingUnits3B, node3.getIpEth(),
                communicationCost3B, new ArrayList<>());
        // Attach worker to Node and COMPSsApplication
        //node3.addWorker(worker3B);
        appB.addWorker(worker3B);

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // CommunicationLink21 definition
        final float delayRtt = 0;
        final float plr = 0;
        final float throughput = 0;
        CommunicationLink communicationLink21 = new CommunicationLink(node1, node2, node1.getIpEth(), node2.getIpEth(),
                delayRtt, plr, throughput);
        node1.addCommunicationLink(communicationLink21);
        node2.addCommunicationLink(communicationLink21);

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // CommunicationLink31 definition
        CommunicationLink communicationLink31 = new CommunicationLink(node1, node3, node1.getIpEth(), node3.getIpEth(),
                delayRtt, plr, throughput);
        node1.addCommunicationLink(communicationLink31);
        node3.addCommunicationLink(communicationLink31);

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // CommunicationLink32 definition
        CommunicationLink communicationLink32 = new CommunicationLink(node2, node3, node2.getIpEth(), node3.getIpEth(),
                delayRtt, plr, throughput);
        node2.addCommunicationLink(communicationLink32);
        node3.addCommunicationLink(communicationLink32);

        ArrayList<Node> systemNodes = new ArrayList<>(Arrays.asList(node1, node2, node3));
        ArrayList<COMPSsApplication> systemApps = new ArrayList<>(Arrays.asList(appA, appB));

        return new ElasticSystem(systemApps, systemNodes);
    }

    public static void main(String[] args)
            throws DataClayException, ObjectNotRegisteredException {

        DataClay.init();

        ElasticSystem system;
        try {
            system = ElasticSystem.getByAlias(SYSTEM_ALIAS);
            System.out.println("ElasticSystem detected");
            System.out.println("\tNodes: " + system.getNodes().stream()
                    .map(n -> String.format("{%s,%s}", n.getIpEth(), n.getIpWifi()))
                    .collect(Collectors.joining(", ")));
        } catch (ObjectNotRegisteredException e) {
            System.out.println("No ElasticSystem detected");
            system = initializeMockData();
            system.makePersistent(SYSTEM_ALIAS);
            System.out.println("New ElasticSystem with mock data was created.");
            System.out.println("\tMock Nodes: " + system.getNodes().stream()
                    .map(n -> String.format("{%s,%s}", n.getIpEth(), n.getIpWifi()))
                    .collect(Collectors.joining(", ")));
        }

        NFRMonitor nfrMonitor = new NFRMonitor(system, Constants.PERIOD);
        nfrMonitor.runMonitor();
    }

}
