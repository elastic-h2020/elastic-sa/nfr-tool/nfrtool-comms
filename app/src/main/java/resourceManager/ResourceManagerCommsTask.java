package resourceManager;

import java.net.SocketException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.google.common.collect.BiMap;
import es.bsc.compss.nfr.model.CommunicationLink;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;
import es.bsc.compss.nfr.model.Worker;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import utils.Constants;
import utils.FileUtils;

public class ResourceManagerCommsTask extends ResourceManager implements MqttCallback {

    private static final String PUBLISH_TOPIC = "konnekt/v3/service/konnektbox-telemetry/peers";
    private static final String SUBSCRIBE_TOPIC = "konnekt/v3/service/konnektbox-telemetry/net";
    private static final String GRM_TOPIC = "violations";

    private MqttClient mqttClient;
    private MqttClient extMQClient;
    private final ObjectMapper mapper;

    private final FileUtils fileUtils;

    private final Node self;

    public ResourceManagerCommsTask(ElasticSystem elasticSystem) throws MqttException, NodeNotFoundException {
        super(elasticSystem);
        this.initializeMqttClient();
        this.mapper = new ObjectMapper();
        this.fileUtils = new FileUtils();
        this.self = getSelf();
        System.out.printf("Found myself in DataClay! I'm node with IPs: {eth: %s, lte: %s, wifi: %s}\n",
                this.self.getIpEth(), this.self.getIpLte(), this.self.getIpWifi());
    }

    private void initializeMqttClient() throws MqttException {
        final String broker = Optional.ofNullable(System.getenv("MQTT_BROKER_URL"))
                .orElse("tcp://localhost:1883");
        final String extBroker = Optional.ofNullable(System.getenv("CENTRAL_MQ_URL"))
                .orElse("tcp://localhost:1883");
        final String clientId = UUID.randomUUID().toString();
        final String extClientId = UUID.randomUUID().toString();

        final MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setCleanSession(true);  // Totally optional

        this.mqttClient = new MqttClient(broker, clientId, new MemoryPersistence());
        this.extMQClient = new MqttClient(extBroker, extClientId, new MemoryPersistence());
        this.mqttClient.setCallback(this);
        this.mqttClient.connect(connectOptions);
        connectOptions.setAutomaticReconnect(true);
        this.extMQClient.connect(connectOptions);
        this.mqttClient.subscribe(SUBSCRIBE_TOPIC);
    }

    @Override
    public void run() {
        if (this.mqttClient.isConnected()) {
            for (Worker worker : this.self.getWorkers()) {
                requestNetworkingInfo(worker);
            }
            System.out.println("Loop executed");
        } else {
            System.out.println("Loop skipped");
        }
    }

    private void requestNetworkingInfo(Worker worker) {
        final List<CommunicationLink> workerLinks = worker.getCommunicationLinksForApplication();
        try {
            final BiMap<String, String> selfIfaces = this.getOwnInterfaces().inverse();

            final Map<String, Set<String>> linkMap = new HashMap<>();
            for (CommunicationLink link : workerLinks) {
                String interfaceName = selfIfaces.get(link.getIpNode1());
                // Check if interface is null or not valid (docker internals, loopback, etc)
                if (interfaceName == null || interfaceName.indexOf("br-") != -1 || interfaceName == "lo") {
                    continue; // TODO: What to do? Just omit, for the moment
                }
                Set<String> workerSet = linkMap.getOrDefault(interfaceName, new HashSet<>());
                workerSet.add(link.getIpNode2());
                linkMap.put(interfaceName, workerSet);
            }

            final String jsonMessageContent = this.mapper.writeValueAsString(linkMap);
            final MqttMessage message = new MqttMessage(jsonMessageContent.getBytes());
            this.mqttClient.publish(PUBLISH_TOPIC, message);
        } catch (JsonProcessingException e) {
            System.err.println("An exception occurred when serializing the message into JSON");
            e.printStackTrace();
        } catch (MqttException e) {
            System.err.println("An exception occurred with the MQTT connection");
            e.printStackTrace();
        } catch (SocketException e) {
            System.err.println("An exception occurred when querying the local interfaces");
            e.printStackTrace();
        }
    }

    @Override
    public boolean cancel() {
        try {
            this.mqttClient.disconnect();
        } catch (MqttException e) {
            // Do nothing
        }
        return super.cancel();
    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.err.println("The connection to the MQTT broker was lost. Attempting reconnection");
        int tries = 5;
        while (tries-- > 0 && !this.mqttClient.isConnected()) {
            try {
                this.mqttClient.connect();
                System.out.println("Connection recovered!");
            } catch (MqttException e) {
                System.err.println("The reconnection failed. (" + e.getMessage() + ") Tries left: " + tries);
                try { Thread.sleep(3000); } catch (Exception e2) { }
                continue;
            }
            break;
        }
        if (!this.mqttClient.isConnected()) {
            System.err.println("All reconnection attempts failed. Exiting NFRTool");
            System.exit(1);
        } else {
            System.out.println("Connection with the MQTT broker is established");
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) {
        final String message = new String(mqttMessage.getPayload());
        System.out.printf("A message has arrived on topic '%s':\n%s\n", topic, message);
        try {
            final TypeReference<Map<String, InterfaceInformationModel>> jsonType = new TypeReference<Map<String, InterfaceInformationModel>>() { };
            final Map<String, InterfaceInformationModel> result = this.mapper.readValue(message, jsonType);
            final BiMap<String, String> ifaces = getOwnInterfaces();

            this.self.getWorkers().forEach(worker -> {
                final String infoNature = worker.getApplication().getInfoNature();
                final List<CommunicationLink> links = worker.getCommunicationLinksForApplication();

                float priority = Float.parseFloat(this.fileUtils.getYmlNodeScalar(Constants.APP_ATTRIB_PATH, Arrays.asList(infoNature, "priority")));
                float norm = Float.parseFloat(this.fileUtils.getYmlNodeScalar(Constants.APP_ATTRIB_PATH, Arrays.asList(infoNature, "norm")));
                float rttmax = Float.parseFloat(this.fileUtils.getYmlNodeScalar(Constants.APP_ATTRIB_PATH, Arrays.asList(infoNature, "rttmax")));

                float commsCost = result.entrySet()
                        .stream()
                        .peek(entry -> entry.getValue()  // Peeking is like doing forEach but you can continue using
                                                         //   the stream afterwards. So we do that.
                                .getRoundTripTime().forEach((ip2, rtt) -> {
                                    final String ip1 = ifaces.get(entry.getKey());
                                    links.stream()
                                            // Filter the CommunicationLinks so that only ip1->ip2 link(s) remain
                                            .filter(link -> link.getIpNode1().equals(ip1) && link.getIpNode2().equals(ip2))
                                            .forEach(link -> { // If there's any, then store the rtt in DataClay
                                                System.out.printf("Updating RTT of link %s->%s to %f\n", ip1, ip2, rtt);
                                                link.setDelayRtt(rtt);
                                                if (rtt>rttmax){
                                                    try {
                                                        System.out.println(String.format("Violation message sent to GRM: {\"workerIP\": %s, \"workerID\": %s, \"dimension\": \"comms\"}", worker.getIp(), worker.getPid()));
                                                        final String jsonMessageContent = String.format("{\"workerIP\": %s, \"workerID\": %s, \"dimension\": \"comms\"}", worker.getIp(), worker.getPid());
                                                        final MqttMessage violationMsg = new MqttMessage(jsonMessageContent.getBytes());
                                                        extMQClient.publish(GRM_TOPIC, violationMsg);
                                                    } catch (MqttException e) {
                                                        System.err.println("An exception occurred with the MQTT publish");
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                    })
                        )
                        // We continue with the stream and take all the RTTs mentioned in the received message and
                        //   put them into one single stream (by flatting the stream of streams)
                        .flatMap(e -> e.getValue().getRoundTripTime().values().stream())
                        // Then all RTTs are reduced following the formula. This is like a sum, but you get to
                        //   manually specify the initial value (0f) and the formula to apply, if any.
                        .reduce(0f, (tmpCost, rtt) ->
                                tmpCost + (priority / norm) - (float)(Math.log(Math.max(0f, 1f - (rtt / rttmax))))
                        );
                System.out.printf("Communication cost of worker %s set to %f\n", worker.getIp(), commsCost);
                worker.setCommunicationCost(commsCost);
            });
        } catch (Exception e) {
            System.err.println("An exception occurred when processing the received MQTT message");
            e.printStackTrace();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        // Do nothing
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class InterfaceInformationModel {
        private @JsonProperty String ip;
        private @JsonProperty String mac;
        private @JsonProperty("rtt_avg") Map<String, Float> roundTripTime;
        private @JsonProperty double rx_MB;
        private @JsonProperty int rx_lost_packets;
        private @JsonProperty int rx_packets;
        private @JsonProperty double tx_MB;
        private @JsonProperty int tx_lost_packets;
        private @JsonProperty int tx_packets;

        public String getIp() {
            return ip;
        }

        public String getMac() {
            return mac;
        }

        public Map<String, Float> getRoundTripTime() {
            return roundTripTime;
        }

        public double getRx_MB() {
            return rx_MB;
        }

        public int getRx_lost_packets() {
            return rx_lost_packets;
        }

        public int getRx_packets() {
            return rx_packets;
        }

        public double getTx_MB() {
            return tx_MB;
        }

        public int getTx_lost_packets() {
            return tx_lost_packets;
        }

        public int getTx_packets() {
            return tx_packets;
        }

        @Override
        public String toString() {
            return String.format(
                "{" +
                "\t\"ip\": \"%s\",\n" +
                "\t\"mac\": \"%s\",\n" +
                "\t\"rtt_ms\": %s,\n" +
                "\t\"rx_MB\": %f,\n" +
                "\t\"rx_lost_packets\": %d,\n" +
                "\t\"rx_packets\": %d,\n" +
                "\t\"tx_MB\": %f,\n" +
                "\t\"tx_lost_packets\": %d,\n" +
                "\t\"tx_packets\": %d\n" +
                "}",
                this.ip, this.mac, this.roundTripTime,
                this.rx_MB, this.rx_lost_packets, this.rx_packets,
                this.tx_MB, this.tx_lost_packets, this.tx_packets
            );
        }
    }

}
