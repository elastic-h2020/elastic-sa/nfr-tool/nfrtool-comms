package resourceManager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;
import java.util.TimerTask;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;

public abstract class ResourceManager extends TimerTask {

    protected ElasticSystem elasticSystem;
    protected ArrayList<COMPSsApplication> apps;
    protected ArrayList<Node> nodes;

    private BiMap<String, String> ownInterfaces;

    public ResourceManager(ElasticSystem elasticSystem) {
        this.elasticSystem = elasticSystem;
        this.apps = this.elasticSystem.getApplications();
        this.nodes = elasticSystem.getNodes();

        this.ownInterfaces = null;
    }

    protected BiMap<String, String> getOwnInterfaces() throws SocketException {
        this.ownInterfaces = HashBiMap.create();
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();

        for (NetworkInterface netint : Collections.list(nets)) {
            // Ignore loopback and docker interfaces
            if (netint.getName() == "lo" || netint.getName().indexOf("br-") != -1 ||
                netint.getName().indexOf("docker") != -1) {
                continue;
            }
            Enumeration<InetAddress> addresses = netint.getInetAddresses();
            for (InetAddress addr : Collections.list(addresses)) {
                if (addr instanceof Inet4Address)
                    System.out.printf("%s: %s\n", netint.getName(), addr.getHostAddress());
                    this.ownInterfaces.put(netint.getName(), addr.getHostAddress());
            }
        }
        return this.ownInterfaces;
    }

    protected Node getSelf() throws NodeNotFoundException {
        try {
            final Set<String> localIps = this.getOwnInterfaces().values();
            System.out.println("Local IPs: " + localIps);
            return elasticSystem.getNodes()
                    .stream()
                    .filter(n -> !Collections.disjoint(localIps, Arrays.asList(n.getIpEth(), n.getIpLte(), n.getIpWifi())))
                    .findAny()
                    .orElseThrow(() -> new NodeNotFoundException(localIps));
        } catch (SocketException e) {
            System.err.println("An error occurred when scanning local network interfaces: " + e.getMessage());
            throw new NodeNotFoundException();
        }
    }

    public static class NodeNotFoundException extends Exception {

        public NodeNotFoundException(Set<String> ips) {
            super("I could not find myself in DataClay using the following detected set of IPs: " + ips);
        }

        public NodeNotFoundException() {
            super("An error occurred and the node could not be found");
        }

    }

}
