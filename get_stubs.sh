#!/bin/bash

set -e

docker-compose -f model/dataclay/docker-compose.yml kill && \
docker-compose -f model/dataclay/docker-compose.yml down -v && \
docker-compose -f model/dataclay/docker-compose.yml up -d

rm -rf ./app/stubs

docker run -it --rm --network dataclay_default -v $PWD/model/cfgfiles:/home/dataclayusr/dataclay/cfgfiles bscdataclay/client:2.5.dev WaitForDataClayToBeAlive 5 1

docker run -it --rm --network dataclay_default -v $PWD/model/cfgfiles:/home/dataclayusr/dataclay/cfgfiles bscdataclay/client:2.5.dev NewAccount ElasticUser ElasticPass

docker run -it --rm --network dataclay_default -v $PWD/model/cfgfiles:/home/dataclayusr/dataclay/cfgfiles bscdataclay/client:2.5.dev NewDataContract ElasticUser ElasticPass ElasticDS ElasticUser

docker run -it --rm --network dataclay_default -v $PWD/model/cfgfiles:/home/dataclayusr/dataclay/cfgfiles -v $PWD/model/model/target/classes:/classes bscdataclay/client:2.5.dev NewModel ElasticUser ElasticPass ElasticNS /classes java

docker run -it --rm --network dataclay_default -v $PWD/model/cfgfiles:/home/dataclayusr/dataclay/cfgfiles -v $PWD/app/stubs:/stubs bscdataclay/client:2.5.dev GetStubs ElasticUser ElasticPass ElasticNS /stubs

cd ./app/stubs && \
jar cf stubs.jar es/bsc/compss/nfr/model && \
su jjvmelastic -c "mvn install:install-file -Dfile=stubs.jar -DgroupId=es.bsc.compss -DartifactId=nfrtool-dataclay-stubs -Dversion=2.0 -Dpackaging=jar -DcreateChecksum=true" && \
chown -R jjvmelastic:jjvmelastic $PWD 
cd -
